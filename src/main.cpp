#include <Arduino.h>
#include <BikeBright.h>

BikeBright bike_bright(LLED, CLED, RLED, LBUTTON, CBUTTON, RBUTTON);

// Holt the time of the last change
int lastChange;

/** Debounce the button input for toggle */
void debounceToggle()
{
  const auto now = millis();
  if (now - lastChange > 500)
    {
      bike_bright.toggle();
      lastChange = now;
    }
}

void setup()
{
  pinMode(RF, INPUT);
  attachInterrupt(digitalPinToInterrupt(bike_bright.cbutton), debounceToggle, FALLING);
  attachInterrupt(digitalPinToInterrupt(bike_bright.lbutton), [] {bike_bright.mode = Left;}, FALLING);
  attachInterrupt(digitalPinToInterrupt(bike_bright.rbutton), [] {bike_bright.mode = Right;}, FALLING);
  lastChange = millis();
}

void loop()
{
  const auto val = analogRead(RF);

  if (val > 400) // When hand is close, right turn signal
    {
      bike_bright.mode = Right;
    }

  else
    {
      if (val > 300) // When hand is farther, left turn signal
        {
          bike_bright.mode = Left;
        }
    }
  bike_bright.loop();
}
