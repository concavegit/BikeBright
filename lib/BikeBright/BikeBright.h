#pragma once

/**
 * Bike light modes.
 */
enum Mode
  {
   Off,
   Left,
   Right,
   Flash,
   Steady
  };

/**
 * Class containing the pins and toggle functions for the bike light.
 * The buttons are active-low.
 */
class BikeBright
{
public:
  /**
   * BikeBright constructor.
   *
   * @param lled the left led pin.
   * @param cled the central led pin.
   * @param rled the right led pin.
   * @param lbutton the left button pin.
   * @param cbutton the central button pin.
   * @param rbutton the right button pin.
   */
  BikeBright(int lled, int cled, int rled, int lbutton, int cbutton, int rbutton);

  /**
   * A single cycle of the lights for the current mode.
   */
  void loop();

  /**
   * Toggle through the modes.
   * Order: Off, Left, Right, Flash, Steady.
   */
  void toggle();

  Mode mode;

  const int lled;
  const int cled;
  const int rled;
  const int lbutton;
  const int cbutton;
  const int rbutton;

private:
  /** Turn all LEDs off. */
  void off();

  /** Alternate between left and central LEDs, right LED off */
  void left();

  /** Alternate between right and central LEDs, left LED off */
  void right();

  /** Alternate between edge and central LEDs, */
  void flash();


  /** Turn all LEDs on */
  void steady();
};
