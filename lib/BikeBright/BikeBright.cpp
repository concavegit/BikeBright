#include <Arduino.h>
#include "BikeBright.h"

BikeBright::BikeBright(int lled, int cled, int rled,
                       int lbutton, int cbutton, int rbutton)
  : lled(lled),
    cled(cled),
    rled(rled),
    lbutton(lbutton),
    cbutton(cbutton),
    rbutton(rbutton),
    mode(Off)
{
  pinMode(lled, OUTPUT);
  pinMode(cled, OUTPUT);
  pinMode(rled, OUTPUT);
  pinMode(lbutton, INPUT_PULLUP);
  pinMode(cbutton, INPUT_PULLUP);
  pinMode(rbutton, INPUT_PULLUP);
}

void BikeBright::loop()
{
  switch (mode)
    {
    case Off:
      off();
      break;

    case Left:
      left();
      break;

    case Right:
      right();
      break;

    case Flash:
      flash();
      break;

    case Steady:
      steady();
      break;
    }
}

void BikeBright::toggle()
{
  switch(mode)
    {
    case Off:
      mode = Left;
      break;

    case Left:
      mode = Right;
      break;

    case Right:
      mode = Flash;
      break;

    case Flash:
      mode = Steady;
      break;

    case Steady:
      mode = Off;
      break;
    }
}

void BikeBright::off()
{
  digitalWrite(lled, LOW);
  digitalWrite(cled, LOW);
  digitalWrite(rled, LOW);
  delay(500);
}

void BikeBright::left()
{
  digitalWrite(lled, HIGH);
  digitalWrite(cled, LOW);
  digitalWrite(rled, LOW);
  delay(500);
  digitalWrite(lled, LOW);
  digitalWrite(cled, HIGH);
  digitalWrite(rled, LOW);
  delay(500);
}

void BikeBright::right()
{
  digitalWrite(lled, LOW);
  digitalWrite(cled, LOW);
  digitalWrite(rled, HIGH);
  delay(500);
  digitalWrite(lled, LOW);
  digitalWrite(cled, HIGH);
  digitalWrite(rled, LOW);
  delay(500);
}

void BikeBright::flash()
{
  digitalWrite(lled, HIGH);
  digitalWrite(cled, LOW);
  digitalWrite(rled, HIGH);
  delay(500);
  digitalWrite(lled, LOW);
  digitalWrite(cled, HIGH);
  digitalWrite(rled, LOW);
  delay(500);
}

void BikeBright::steady()
{
  digitalWrite(lled, HIGH);
  digitalWrite(cled, HIGH);
  digitalWrite(rled, HIGH);
  delay(500);
}
