EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:teensy
LIBS:bikebright-cache
EELAYER 25 0
EELAYER END
$Descr User 5906 5906
encoding utf-8
Sheet 1 1
Title "BikeBright"
Date "2018-08-31"
Rev "0.0.0"
Comp "ConcaveTriangle"
Comment1 "Switches are INPUT_PULLUP"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Teensy3.6 U1
U 1 1 5B89894E
P 2750 2850
F 0 "U1" H 2750 5150 60  0000 C CNN
F 1 "Teensy3.6" H 2750 550 60  0000 C CNN
F 2 "" H 2750 2850 60  0000 C CNN
F 3 "" H 2750 2850 60  0000 C CNN
	1    2750 2850
	0    -1   -1   0   
$EndComp
$Comp
L R left1
U 1 1 5B898B4A
P 3300 1700
F 0 "left1" V 3380 1700 50  0000 C CNN
F 1 "140" V 3300 1700 50  0000 C CNN
F 2 "" V 3230 1700 50  0001 C CNN
F 3 "" H 3300 1700 50  0001 C CNN
	1    3300 1700
	0    1    1    0   
$EndComp
$Comp
L R center1
U 1 1 5B898C27
P 3300 1400
F 0 "center1" V 3380 1400 50  0000 C CNN
F 1 "140" V 3300 1400 50  0000 C CNN
F 2 "" V 3230 1400 50  0001 C CNN
F 3 "" H 3300 1400 50  0001 C CNN
	1    3300 1400
	0    1    1    0   
$EndComp
$Comp
L R right1
U 1 1 5B898C69
P 3300 1100
F 0 "right1" V 3380 1100 50  0000 C CNN
F 1 "140" V 3300 1100 50  0000 C CNN
F 2 "" V 3230 1100 50  0001 C CNN
F 3 "" H 3300 1100 50  0001 C CNN
	1    3300 1100
	0    1    1    0   
$EndComp
$Comp
L LED right2
U 1 1 5B898E0B
P 3600 1100
F 0 "right2" H 3600 1200 50  0000 C CNN
F 1 "LED" H 3600 1000 50  0000 C CNN
F 2 "" H 3600 1100 50  0001 C CNN
F 3 "" H 3600 1100 50  0001 C CNN
	1    3600 1100
	1    0    0    -1  
$EndComp
$Comp
L LED center2
U 1 1 5B898E5A
P 3600 1400
F 0 "center2" H 3600 1500 50  0000 C CNN
F 1 "LED" H 3600 1300 50  0000 C CNN
F 2 "" H 3600 1400 50  0001 C CNN
F 3 "" H 3600 1400 50  0001 C CNN
	1    3600 1400
	1    0    0    -1  
$EndComp
$Comp
L LED left2
U 1 1 5B898E8D
P 3600 1700
F 0 "left2" H 3600 1800 50  0000 C CNN
F 1 "LED" H 3600 1600 50  0000 C CNN
F 2 "" H 3600 1700 50  0001 C CNN
F 3 "" H 3600 1700 50  0001 C CNN
	1    3600 1700
	1    0    0    -1  
$EndComp
$Comp
L SW_DIP_x03 buttons1
U 1 1 5B899084
P 4750 1550
F 0 "buttons1" H 4750 1900 50  0000 C CNN
F 1 "SW_DIP_x03" H 4750 1400 50  0000 C CNN
F 2 "" H 4750 1550 50  0001 C CNN
F 3 "" H 4750 1550 50  0001 C CNN
	1    4750 1550
	0    -1   -1   0   
$EndComp
$Comp
L TSSP58P38 Rangefinder1
U 1 1 5B899C20
P 2150 1200
F 0 "Rangefinder1" H 1750 1500 50  0000 L CNN
F 1 "TSSP58P38" H 1750 900 50  0000 L CNN
F 2 "Opto-Devices:IRReceiver_Vishay_MINICAST-3pin" H 2100 825 50  0001 C CNN
F 3 "" H 2800 1500 50  0001 C CNN
	1    2150 1200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4250 1850 4250 1700
Wire Wire Line
	4250 1700 3750 1700
Wire Wire Line
	4350 1850 4350 1400
Wire Wire Line
	4350 1400 3750 1400
Wire Wire Line
	4450 1850 4450 1100
Wire Wire Line
	4450 1100 3750 1100
Wire Wire Line
	3150 1850 3150 800 
Wire Wire Line
	2350 800  4750 800 
Wire Wire Line
	4750 800  4750 1250
Wire Wire Line
	4650 1250 4650 800 
Connection ~ 4650 800 
Wire Wire Line
	4550 1250 4550 800 
Connection ~ 4550 800 
Connection ~ 3150 800 
Wire Wire Line
	1950 800  1750 800 
Wire Wire Line
	1750 800  1750 1850
Wire Wire Line
	2150 800  2150 650 
Wire Wire Line
	2150 650  5000 650 
Wire Wire Line
	5000 650  5000 1850
Wire Wire Line
	5000 1850 4850 1850
$EndSCHEMATC
