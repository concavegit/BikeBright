* Bikelight
This prototyped bike light has 5 modes: off, left, right, flash, and steady.
If equipped with a distance sensor, it turns on the left or right signal according to the distance of your hand from the sensor.

* Instructions

** Hardware
The KiCad files are in =/eda=, Here is the wiring diagram for the teensy3.6:

[[file:res/bikebright.jpg]]

This is what it looks like wired up:

[[file:res/prototype.jpg]]

** Software
Make sure you have [[https://platformio.org/][PlatformIO]] installed.

1. Clone this [[https://gitlab.com/concavegit/BikeBright.git][repo]].
2. Navigate to the cloned folder in the command line.
3. Plug in the teensy3.6
4. Run =pio run -t upload=
